def browseDict(keyElt, dictionary):
    NOT_FOUND = ""
    for key, value in dictionary.items():        
        if (key == keyElt):
            return (keyElt, value)
        if (isinstance(value,dict)):
            result = browseDict(keyElt, value)
            if(result != NOT_FOUND):
                return result
    return NOT_FOUND  

def get_inputs_and_secrets(dictionary):
    variables = {}
    result = browseDict("workflow_call", dictionary)
    if result:
        _, value = result
        inputs = value.get("inputs", "")
        secrets = value.get("secrets", "")
        if(inputs):
            for key, value in inputs.items():
                variables[key] = format_content(value.get("default", "")) \
                    if value.get("type") == "string" else value.get("default", "")
        if(secrets):
            for key, value in secrets.items():
                variables[key] = format_content(value.get("default", "")) \
                    if value.get("type") == "string" else value.get("default", "")
    return variables

def get_job_env_variables(job):
    variables = {}
    env = job.get("env", {})
    if(env):
        for key, value in env.items():
            variables[key] = format_content(value)
    return variables

def get_docker_image(job):
    container = job.get("container", "")
    if(container):
        if(isinstance(container, str)):
            return container
        elif(isinstance(container, dict)):
            return container.get("image", "")
    else:
        _, steps = browseDict("steps", job)
        tech, version = get_setup(steps)
        if tech:
            if version:
                tech = tech + ":" + version
            return tech
    return ""
    
def get_runs_on(job):
    return job.get("runs-on", "")

def get_steps(job):
    return job.get("steps", [])

def get_artifacts(steps):
    artifacts = {
        "name" : "",
        "paths" : []
    }
    for elt in steps:
        if "actions/upload-artifact" in elt.get("uses", ""):
             # concatane format_content(elt["with"].get("name", "") and artifacts["name"] iff this last isn't
             # empty and not whitespace
            artifacts["name"] = (
                format_content(elt["with"].get("name", "")), 
                artifacts["name"] + " - " + format_content(elt["with"].get("name", ""))
                )[ not artifacts["name"].isspace() and len(artifacts["name"]) > 0]           
            artifacts["paths"].extend([format_content(line) 
                                    for line in elt["with"].get("path", "").splitlines()])
            artifacts["expire_in"] = elt["with"].get("retention-days", "")
            when = elt.get("if", "")
            if when == "failure()":
                artifacts["when"] = "on_failure"
            elif when == "always()":
                artifacts["when"] = "always"
            else:
                artifacts["when"] = "on_success"
                
    return artifacts
    
def get_cache(steps):
    cache = {}
    for elt in steps:
        if "actions/cache" in elt.get("uses", ""):
            cache["key"] = elt["with"].get("key", "")
            cache["paths"] = [format_content(line) 
                                for line in elt["with"].get("path", "").splitlines()]
    return cache

def get_script(steps):
    script = []
    for step in steps:
        if "run" in step:
            lines = step.get("run").splitlines()
            for line in lines:
                script.append(format_content(line))
    return script

def format_content(string):
    elt = string.split(">>")[0].strip().replace("${{", "${").replace("}}", "}")\
        .replace("env.","").replace("inputs.", "").replace("secrets.", "")
    return elt

def get_matrix(job):
    parallel_matrix = []
    orphan_elts = {}
    result = browseDict("matrix", job)
    if result: # Test if we didn't receive an empty string but a tuple instead
        _, strategy_matrix = result
        for elt in strategy_matrix:
            if elt not in ("exclude", "include", "fail_fast"):
                orphan_elts[elt] = strategy_matrix[elt]
            if elt == "include":
                parallel_matrix.extend(strategy_matrix[elt])
        if orphan_elts:
            parallel_matrix.insert(0, orphan_elts)
    return parallel_matrix

def get_setup(steps):
    tech = ""
    version = ""
    for elt in steps:
        if "actions/setup" in elt.get("uses", ""):
            tech = elt.get("uses").split("setup-")[1].split("@")[0] 
            with_key= elt.get("with", "")
            if with_key:
                version_key = tech + "-version"
                version = elt["with"].get(version_key, "")
    return (tech, version)

def github_to_gitlab(workflow_content):
    gitlab_cicd_content = {}
    variables = get_inputs_and_secrets(workflow_content)
    _,jobs = browseDict("jobs", workflow_content)
    for job_name, job in jobs.items():
        gitlab_cicd_content[job_name] = {}
        job_variables = get_job_env_variables(job)
        variables.update(job_variables)
        image = get_docker_image(job)
        steps = get_steps(job)
        cache = get_cache(steps)
        script = get_script(steps)
        gitlab_cicd_content[job_name]["script"] = script
        artifacts = get_artifacts(steps)
        matrix = get_matrix(job)
        if image:
            gitlab_cicd_content[job_name]["image"] = image
        if variables:
            gitlab_cicd_content[job_name]["variables"] = variables
        if cache:
            gitlab_cicd_content[job_name]["cache"] = cache
        if artifacts["paths"]:
            gitlab_cicd_content[job_name]["artifacts"] = artifacts
        if matrix:
            gitlab_cicd_content[job_name]["matrix"] = matrix
    return gitlab_cicd_content