# Blocking points for GitHub Actions worflow conversion to GitLab CI/CD pipeline

At this point we try to convert a GitHub Actions workflow into a Gitlab CI/CD pipeline programmatically. Some drawbacks and limitations of our imperative programme came out. The following blocking points are non exhaustive.

- Current impossibility to take into account actions, particularly community ones
- GitHub Actions has many predefined environment variables and contexts which don't have any equivalent in GitLab CI/CD and vice-versa.
- There are several keywords in each platform with no equivalent.
- Limitation for converting workflow with multiple cache or upload-artifacts with differend parameters values
- Difficulty to manage the update of variables and the directories changes
  
## Actions conversion issues

An action in GitHub Actions jargon is a piece of actions/instructions delegated to an external code written by you or another one. It can be an official action maintained by GitHub (actions with owner named `actions` such as `actions/cache`, `actions/setup-node`) or tier/community actions.
There are three types of actions, from the less to the most implemented and used : composite actions, docker container and javascript actions. Every action require a metadata file to define the inputs, outputs and main entrypoint for your action.The metadata filename must be either action.yml or action.yaml. For more information, see ["Metadata syntax for GitHub Actions."](https://docs.github.com/en/actions/creating-actions/about-custom-actions#:~:text=%22Metadata%20syntax%20for%20GitHub%20Actions.%22).
Here are some examples:

- A javascript action `gr2m/merge-schedule-action` with ["metadata file](https://github.com/gr2m/merge-schedule-action/blob/master/action.yml) and ["main entrypoint (action implementation code)"](https://github.com/gr2m/merge-schedule-action/blob/master/dist/index.js).
- A docker container action, follow ["the documentation"](https://docs.github.com/en/actions/creating-actions/creating-a-docker-container-action)

!!! Info
    An `action` isn't a job but just an instruction within the steps section of a job like the `run` statement.

```yml
name: <workflow name> # Optional
on: <one or more event that trigger the workflow> 
jobs: # section where to define all jobs of the workflow
    first-job: # job ID / name to identify it
        name: <text to shortly describe the job purpose> # Optional
        runs-on: <[ubuntu|windows|macos]-version> # Type of machine to run the job on
        steps: # Instructions and Actions to run in the job
            - name: Checkout 
              uses: actions/checkout@v2 # An action alwalys used to import repository content inside the runner environment
            - uses: <{owner}/{action name}@{ref}> # Another acction to run
            - run: <shell cmd> # Command to execute
    second-job:
        ...
```

Nowadays, we can't manage workflows that contain `actions`. Some questions come out such as `"How to treat them ?"`, `"Knowing that GitLab only use _shell script_ which is equivalent to _run_ step in GitHub, is it possible to convert or launch an external execution of that javascript or docker container actions ?"`
Some actions like `actions/cache`, `actions/upload-artifacts` are convert by using respectively `cache` and `artifacts` in GitLab. Action `actions/checkout` is ignored when converting, GitLab automatically load the repository content inside the runner environnement. For `setup` actions family which configure the runner environment, such as `actions/setup-java`, `actions/setup-node`, etc., it seems possible to simulate equivalent behavior by dynamically identify the technology and run equivalent shell script to install it and its dependencies. We need to explore that way or use an official docker image in which it's installed.

## Predefined variables and keywords without equivalent

Several GitHub Actions (resp. GitLab CI/CD) predefined environment variables and keywords such as `GITHUB_ENV`, `GITHUB_WORKFLOW`, `GITHUB_WORKSPACE`, `concurrency` (resp. `CI_MERGE_REQUEST_TARGET_BRANCH_NAME`, `CI_MERGE_REQUEST_SOURCE_BRANCH_NAME`, `stages`, `include`) don't have any equivalent. So, are impacts of ignoring / discarding during conversion those parts important ?

## Case of multiple `actions/uploads` defined

When we encounter multiple `actions/uploads` steps in a workflow with different parameters values, we can't properly convert them in GitLab one by one. GitLab will complain duplicated keys errors.
For example, the following GitHub Actions workflow snippet:

```yml
steps:
    ...
    - name: Archive Cypress videos
    uses: actions/upload-artifact@v2
    if: always() 
    with:
        # Artifact name. Optional, default is artifact
        name: cypress-videos
        path: cypress/videos
        if-no-files-found: error
        retention-days: 15
    - name: Archive Cypress screenshots when job failed
    uses: actions/upload-artifact@v2
    if: failure()
    with:
        # Artifact name. Optional, default is artifact
        name: cypress-screenshots
        path: cypress/screenshots
        if-no-files-found: error
        retention-days: 15
```

can't be convert neither in the following GitLab CI/CD snippet code:

```yml
artifacts:
    when: always
    name: Archive Cypress videos
    paths:
      - cypress/videos/**/*.mp4
artifacts: # duplicated key
    when: on_failure
    name: Archive Cypress screenshots when job failed
    paths:
      - cypress/screenshots/**/*.png
```

or

```yml
artifacts:
    when: always
    name: Archive Cypress videos
    paths:
      - cypress/videos/**/*.mp4
    when: on_failure # duplicated key
    name: Archive Cypress screenshots when job failed # duplicated key
    paths: # duplicated key
      - cypress/screenshots/**/*.png
```

We have to make some choices and trade-offs to get a unique `artifacts` section like the following:

```yml
artifacts:
    expire_in: 15
    name: cypress-videos - cypress-screenshots
    paths:
    - cypress/videos
    - cypress/screenshots
    when: on_failure
```

## GitHub Actions specificities

GitHub allows to evaluate expression by using the specific syntax `${{ <expression> }}`. GitHub Actions `expression` can be any combination of litterals, context references, and functions using operators which is not the case in GitLab. See more in the [doc | Expressions](https://docs.github.com/en/actions/learn-github-actions/expressions).
